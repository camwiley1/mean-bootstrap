var mongoose = require('mongoose'),
    db,
    dbName = '{app-name}';

mongoose.connect('mongodb://localhost/' + dbName);

db = mongoose.connection;

db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function (callback) {
    console.log('open');
});

module.exports = mongoose;
